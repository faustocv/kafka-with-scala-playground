# Kafka with Scala Playground 

## Prerequisites

- Docker
- Docker-Compose
- Scala
- SBT

## Starting up Kafka cluster

```bash
docker-compose up -d --build
```

## Working with Scala project

```bash
cd kafka-exercises
sbt
```
